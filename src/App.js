import Search from './components/Search/index'
import FilmList from './components/FilmList/index'
import Film from './components/Film/index'

export default Vue.component('app', {
    data: () => ({
        films: null,
        error: false,
        errorMessage: null
    }),
    components: { Search, FilmList, Film },
    template: `
        <div>
            <search @error="setError" @films="setFilms"></search>
            <filmlist title="Films" v-if="films" :films="films"></filmlist>
            <span v-if="error">{{ errorMessage }}</span>
        </div>
    `,
    methods: {
        setFilms(films){
            this.films = films
        },
        setError({ message }){
            this.error = true
            this.errorMessage = message
        }
    },
})