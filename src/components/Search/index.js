import { Api } from '../../services/Api'

export default Vue.component('search', {
    data: () => ({
        searchInput: ''
    }),
    methods: {
        async search(){
            let query = this.searchInput
            if (this.searchInput.length === 0){
                this.$emit('error', { message: 'Add value in input' })
                return
            }
            if (this.searchInput.split(' ').length > 1){
                query = this.searchInput.split(' ').join('+')
            }
            try {
                const films = await Api.getFilmsByQuery(`?s=${query}`)
                if (films.Error){
                    this.$emit('error', { message: 'Not found' })
                } else {
                    this.$emit('films', films)
                }
            } catch (error){
                this.$emit('error', { message: 'Client error' })
            }
        }
    },
    template: `
    <div class="search">
        <input type="text" @keyup.enter="search" v-model="searchInput">
        <button @click="search">Search</button>
    </div>
    `
})