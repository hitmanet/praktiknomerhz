import { favoriteMovies } from '../../helpers'
import './style.css'



export default Vue.component('film', {
    props: {
        film: Object,
    },
    template: `
        <div>
            <img :src = "film.Poster !== 'N/A' ? film.Poster : './noimg.jpg'" alt = 'No film poster'>
            <span class="film__title">{{ film.Title }}</span>
            <span class="film__year">{{ film.Year }}</span>
        </div>
    `
})
