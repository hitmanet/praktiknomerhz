import Film from '../Film/index'

export default Vue.component('filmlist', {
    template: `
        <div>
            <h2>{{ title }}</h2>
            <div v-for="(film, key) in films" :key="key">
                <film :film="film"></film>
            </div>
        </div>
    `,
    props: {
        films: Array,
        title: String
    },
    components: { Film }
})